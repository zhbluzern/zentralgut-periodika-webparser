Laut Chat.GPT kann Selenium auch ohne Browser `headless` ausgeführt werden.

```python
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

options = Options()
options.add_argument("--headless")  # Konfiguriert Chrome für den Headless-Modus
options.add_argument("--no-sandbox")  # Fügt das No-Sandbox-Argument hinzu, wichtig für Linux-Server
options.add_argument("--disable-dev-shm-usage")  # Verhindert einige Probleme im Zusammenhang mit dem Shared Memory

driver = webdriver.Chrome(executable_path="/path/to/chromedriver", options=options)

driver.get("http://example.com")
print(driver.title)  # Gibt den Titel der Webseite aus

driver.quit()
```

