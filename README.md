# ZentralGut Periodika WebParser

This repository contains several Jupyter Notebooks for quick and easy scraping of different Journal websites to get the PDF for further ingesting in ZentralGut.ch

Each Journal has its own webdesign which makes need to adopt the HTML-Parser individually. 

## Available Journals

Currently the following journals could be scraped

### Quartierszeitungen

* Hochwacht-Post
* Euses Quartier (Wesemlin)
* Maihof
* Obergrund

### Hochschul-Zeitschriften

* HSLU: Das Magazin (eingestellt)
* UniLuzern: UniLuz Aktuell (eingestellt), Cogito

### Misc

* Luzerner Kantonsblatt
* Zuger Neujahrsblatt
* Kulturjournal Obwalden-Nidwalden
* Entomologische Berichte Luzerns (via Zobodat.at)