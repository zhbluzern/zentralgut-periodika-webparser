from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
import urllib.request
import re
import pandas as pd
import os
from urllib.parse import urlparse
import src.ePerioVolume as eVol
import time 

def getHTML(url):
    baseurl = url
    req = Request(baseurl, headers={'User-Agent': 'Webparsing ZentralGut.ch operated by mailto:christian.erlinger@zhbluzern.ch'})
    contents = urlopen(req).read()
    soup = BeautifulSoup(contents, 'html.parser')
    return soup

def downloadPdf(pdfUrl, target):
    urllib.request.urlretrieve(pdfUrl, target)

baseUrl = "https://www.e-periodica.ch"
tocUrl = f"{baseUrl}/digbib/volumes?UID=hnu-001"

rootHtml = getHTML(tocUrl)
resultSet = []
resultSet_withArticle = []
volumeList = rootHtml.find_all("li",{"class":"ep-volumes__list__item"})

for volI, vol in enumerate(volumeList):
    #print(volI)
    resultDet = {}
    link = vol.find("a")
    resultDet["volLink"] = f"{baseUrl}{link['href']}"
    volIdent = vol.find("article")
    resultDet["volIdentifier"]=volIdent["data-pid"]
    currentNo = vol.find("h3", {"class": "ep-volumes__volume__caption"}).find("span")
    resultDet["currentNo"] = currentNo.text

    tocUrl = f"{baseUrl}/digbib/view?pid={resultDet['volIdentifier']}"
    tocHtml = getHTML(tocUrl)

    articleList = tocHtml.find("table",{"class":"ep-view__toc__table"}).find("tbody").find_all("tr")
    for article in articleList:
        resultDet_Article = {}
        resultDet_Article.update(resultDet)
        resultDet_Article.update(eVol.parseArticle(article))
        print(resultDet_Article)
        resultSet_withArticle.append(resultDet_Article)
        #time.sleep(2)

    #print(resultSet_withArticle)
    #print(resultDet)
    resultSet.append(resultDet)
    #break

df = pd.DataFrame(resultSet) 
df.to_excel('E_Periodica/Hist_Neujahrsblatt_UR.xlsx', index=False)

df = pd.DataFrame(resultSet_withArticle) 
df.to_excel('E_Periodica/Hist_Neujahrsblatt_UR_withArticles.xlsx', index=False)