# Parse E-Periodica

e-periodica.ch bietet zwar eine OAI-Schnittstelle an, in der Metadaten und das PDF-File für einzelne strukturierte Artikel geladen werden können, allerdings sind OAI-Sets nicht auf Journalebene vorhanden (nur auf DDC-Hauptgruppenebene); daher ist es nötig, um ein ganzes Journal zu laden, über die Website zuerst die Bandübersicht zu laden und darauf aufbauend den PDF-Download und den Metadatenzugriff via OAI für die Strukturelemente durchzuführen.

## Post-Production

1. Crop the e-periodica cover page `for i in *.pdf; do pdftk $i cat 2-end output cropped/$i; done output cropped/$i; done`
2. merge the article pdfs to one volume file: `pdfunite $(ls {VolumePrefix}*.pdf) {VolumePrefix}.pdf`