#!/usr/bin/bash

# Directory containing the PDF files
source_directory="Files/cropped/"
target_directory="../../Files/merged" #seen from source directory, therefore ../../

# Go to the directory where the files are located
cd "$source_directory" || exit

# List distinct prefixes by pattern grouping
for prefix in $(ls hnu_001_[0-9][0-9][0-9][0-9]_*.pdf | awk -F'__' '{print $1}' | sort | uniq); do
    # Extract the prefix
    echo "Processing prefix: $prefix"

    # Create the output filename
    output_file="${target_directory}/${prefix}.pdf"
    
    # Merge all PDF files with the current prefix into a single file
    pdfunite  $(ls $prefix*.pdf) "$output_file"
    
    echo "Merged PDF saved to: $output_file"    
done