from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
import urllib.request
import re
import pandas as pd
import os
import urllib.parse
import requests
from lxml import etree
import PyPDF2

def getNumOfPdfPages(filePath):
    # opened file as reading (r) in binary (b) mode
    file = open(filePath, 'rb')
    # store data in pdfReader
    pdfReader = PyPDF2.PdfReader(file)
    # count number of pages
    totalPages = len(pdfReader.pages)
    # print number of pages
    print(f"Total Pages of {filePath}: {totalPages}")
    return totalPages

def getHTML(url):
    baseurl = url
    req = Request(baseurl, headers={'User-Agent': 'Webparsing ZentralGut.ch operated by mailto:christian.erlinger@zhbluzern.ch'})
    contents = urlopen(req).read()
    soup = BeautifulSoup(contents, 'html.parser')
    return soup

def downloadPdf(pdfUrl, target):
   #print(urllib.request.urlretrieve(pdfUrl, target))
   #urllib.request.urlretrieve(pdfUrl, target)
    response = requests.get(pdfUrl)
    if response.status_code == 200:
        pdf = open(target, 'wb')
        pdf.write(response.content)
        pdf.close()
    return response.status_code


def getArticlePID(articleUrl):
    regex = r"cntmng\?pid=(.+)"
    matches = re.finditer(regex, articleUrl, re.MULTILINE)
    for matchNum, match in enumerate(matches, start=1):
        return urllib.parse.unquote(match.group(1))

def buildList(node, resultList):
    nodeName = etree.QName(node).localname
    res = {key: val for key, val in resultList.items() if key.startswith(f"dc:{nodeName}")}
    #print (len(res))
    keyName = f"dc:{nodeName}_{len(res)}"
    resultList[keyName] = node.text
    return resultList

def getArticleDataByOAI(articleID, resultArticle):
    oaiUrl = f"https://www.e-periodica.ch/oai?verb=GetRecord&identifier=oai:agora.ch:{articleID}&metadataPrefix=oai_dc"
    print(oaiUrl)
    r = requests.get(url=oaiUrl, headers={'User-Agent': 'Webparsing ZentralGut.ch operated by mailto:christian.erlinger@zhbluzern.ch'})
    namespaces = { "xmlns" : "http://www.openarchives.org/OAI/2.0/", "dc":"http://purl.org/dc/elements/1.1/", "oai_dc" : "http://www.openarchives.org/OAI/2.0/oai_dc/"}
    root = etree.fromstring(r.content)
    Metadata = root.xpath(".//oai_dc:dc", namespaces=namespaces)
    if Metadata != []:
        for node in Metadata[0].getchildren():
            buildList(node, resultArticle)
            #break
        #print(resultArticle)
    return resultArticle


def parseArticle(article):
        baseUrl = "https://www.e-periodica.ch"
        resultArticle = {}
        links = article.find("td", {"class":"hierarchy-20 ep-view__toc__node--dl ep-view__toc__node--no-child"})
        pdfUrl = links.find("a", {"class": "ep-view__toc__pdf"})
        print(pdfUrl["href"])
        articleId=getArticlePID(pdfUrl["href"])
        localFileName = re.sub("-|:","_",articleId)
        fileUrl = f"{baseUrl}{pdfUrl['href']}"
        fileUrl = re.sub("digbib/../cntmng\?","cntmng?type=pdf&",fileUrl)
        resultArticle["fileUrl"] = fileUrl
        resultArticle["fileName"] = f"{localFileName}.pdf"
        resultArticle["fileDownloadStatus"] = downloadPdf(fileUrl, f"E_Periodica/Files/{localFileName}.pdf")
        if resultArticle["fileDownloadStatus"] == 200:
            resultArticle["numOfPages"] = getNumOfPdfPages(f"E_Periodica/Files/{localFileName}.pdf")
        articleTitle = links.find("a", {"class":"ep-view__toc__unit"})
        resultArticle["title"]=articleTitle.text.strip()
        #articleId = "hnu-001:2017:108::144"
        getArticleDataByOAI(articleId, resultArticle)
        #break
        #print(resultArticle)
        return resultArticle



if __name__ == "__main__":
    baseUrl = "https://www.e-periodica.ch"
    tocUrl = f"{baseUrl}/digbib/view?pid=hnu-001%3A2017%3A108#4"
    rootHtml = getHTML(tocUrl)
    resultSet = []

    articleList = rootHtml.find("table",{"class":"ep-view__toc__table"}).find("tbody").find_all("tr")
    for article in articleList:
        parseArticle(article)