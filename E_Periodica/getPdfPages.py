import PyPDF2
import pandas as pd

def getNumOfPdfPages(filePath):
    # opened file as reading (r) in binary (b) mode
    file = open(filePath, 'rb')
    # store data in pdfReader
    pdfReader = PyPDF2.PdfReader(file)
    # count number of pages
    totalPages = len(pdfReader.pages)
    # print number of pages
    print(f"Total Pages of {filePath}: {totalPages}")
    return totalPages

if __name__ == "__main__":
    #getNumOfPdfPages("Files/cropped/hnu_001_1975_66__213.pdf")
    data = pd.read_excel("Hist_Neujahrsblatt_UR_withArticles.xlsx")
    #df = pd.DataFrame(data, columns=['VorgangID','UseAndReproductionLicense'])
    df = pd.DataFrame(data)
    df.fillna("", inplace=True)
    print (df.head)

    #Schleife durch das Excel-Input-File, konkret den pandaisierten DataFrame.
    pageNums = []
    for index, row in df.iterrows():
        print(row["fileName"])
        pageNums.append(getNumOfPdfPages(f"Files/cropped/{row['fileName']}"))

    df_updated = df.assign(numOfPages=pageNums)
    print(df_updated.head())
    df_updated.to_excel('Hist_Neujahrsblatt_UR_withArticlesPageNums.xlsx', index=False)
