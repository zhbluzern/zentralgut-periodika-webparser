from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
import urllib.request
import re
import pandas as pd
import os
from urllib.parse import urlparse

def getHTML(url):
    baseurl = url
    req = Request(baseurl, headers={'User-Agent': 'Webparsing ZentralGut.ch operated by mailto:christian.erlinger@zhbluzern.ch'})
    contents = urlopen(req).read()
    soup = BeautifulSoup(contents, 'html.parser')
    return soup

def downloadPdf(pdfUrl, target):
    urllib.request.urlretrieve(pdfUrl, target)

def getFileName(fileUrl):
    a = urlparse(link["href"])
    #print(a.path)                
    #print(os.path.basename(a.path))
    #print(os.path.splitext(os.path.basename(a.path)))
    return a.path, os.path.basename(a.path), os.path.splitext(os.path.basename(a.path))


baseUrl = "https://denkmalpflege.lu.ch"
rootUrl = f"{baseUrl}/Berichte_ab_2010"
rootHtml = getHTML(rootUrl)
resultSet = []

list = rootHtml.find_all("li", {"class":"navigation__child-list-item"}) 


# for link in links:
for item in list:
    link = item.find("a", {"class": "navigation__child-list-item-link"})
    print(f"{baseUrl}{link['href']}")
    targetHtml = getHTML(f"{baseUrl}{link['href']}")
    pdfUrl = targetHtml.find("a", href=re.compile(f'\.pdf'))
    if pdfUrl != None:
        resultDet = {}
        print(f"{baseUrl}{pdfUrl['href']}")
        fileName = getFileName(f"{baseUrl}{pdfUrl['href']}")
        print(fileName[1])
        localFileName = f"{fileName[1]}.pdf"
        downloadPdf(f"{baseUrl}{pdfUrl['href']}", f"LU_Berichte_Denkmalpflege/Files/{localFileName}")
        resultDet["issue"] = fileName[1]
        resultDet["fileName"] = localFileName
        resultSet.append(resultDet)

df = pd.DataFrame(resultSet) 
df.to_excel('LU_Berichte_Denkmalpflege/Berichte_Denkmalpflege.xlsx', index=False)
