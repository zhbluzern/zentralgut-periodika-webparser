from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
import urllib.request
import re
import pandas as pd
import os
from urllib.parse import urlparse

def getHTML(url):
    baseurl = url
    req = Request(baseurl, headers={'User-Agent': 'Webparsing ZentralGut.ch operated by mailto:christian.erlinger@zhbluzern.ch'})
    contents = urlopen(req).read()
    soup = BeautifulSoup(contents, 'html.parser')
    return soup

def downloadPdf(pdfUrl, target):
    urllib.request.urlretrieve(pdfUrl, target)

def getFileName(fileUrl):
    a = urlparse(link["href"])
    #print(a.path)                
    #print(os.path.basename(a.path))
    #print(os.path.splitext(os.path.basename(a.path)))
    return a.path, os.path.basename(a.path), os.path.splitext(os.path.basename(a.path))

#baseUrl = "https://www.muri-gries.ch/mediawiki/index.php/Sarner_Kollegi_Chronik"
#baseUrl = "https://www.muri-gries.ch/mediawiki/index.php/Jahresbericht_der_Kantonsschule"
baseUrl = "https://www.muri-gries.ch/mediawiki/index.php/Beilage_zum_Jahresbericht"
rootHtml = getHTML(baseUrl)
resultSet = []

#links = rootHtml.find_all("a", {"class": "external text"}) # Kolleg-Chronik
#links = rootHtml.find("ul").find_all("a", {"class": "external text"}) # JB direkt to linkt
list = rootHtml.find("ul").find_all("li") # JB first <li>

# for link in links:
for item in list:
    link = item.find("a", {"class": "external text"})
    if link != None:
        resultDet = {}
        resultDet["title"] = item.text
        print(f"{link.text} {link['href']}")
        resultDet["originUrl"] = link["href"]
        resultDet["issue"] = link.text
        fileName = getFileName(link["href"])
        localFileName = re.sub("\.|-","_",fileName[2][0])+fileName[2][1]
        print(localFileName)
        resultDet["fileName"] = localFileName
        downloadPdf(link["href"], f"OW_prioratSarnen/Files_BeilageKanti/{localFileName}")
        resultSet.append(resultDet)
    #break
df = pd.DataFrame(resultSet) 
df.to_excel('OW_prioratSarnen/KantiJB_Beilage.xlsx', index=False) 
    